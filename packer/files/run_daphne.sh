#!/usr/bin/env bash

. .env.sh

# set redis address for the main server
export REDIS_PORT_6379_TCP_ADDR=localhost


python3 ./manage.py collectstatic --noinput
python3 ./manage.py migrate

daphne -b 0.0.0.0 -p 8080 gamepad.asgi:application --access-log /var/log/django/daphne.log
