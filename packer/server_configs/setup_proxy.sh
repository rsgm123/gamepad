#!/usr/bin/env bash


TMP_DIR=/tmp/gamepad
APP_DIR=/home/django/gamepad
SSH_DIR=/home/django/.ssh

cd $APP_DIR

# setup caddy
sudo apt-get install -y less htop

curl https://getcaddy.com | bash -s personal
sudo mkdir /var/log/caddy
