#!/usr/bin/env bash


TMP_DIR=/tmp/gamepad
APP_DIR=/home/django/gamepad
SSH_DIR=/home/django/.ssh

apt-get update
apt-get install -y python python-dev python3 python3-dev gcc less htop jq git

# setup user
useradd -m django

mkdir $TMP_DIR
tar -xf /tmp/gamepad.tar.gz -C $TMP_DIR


# add config files not in git
# cp -r will try to put files in DEST/DIR_NAME, so just specify home here
cp -rv $TMP_DIR /home/django

chown -R django:django $APP_DIR
chmod +x $APP_DIR/.env.sh

mkdir /var/log/django
chown django:django /var/log/django


# install dependencies
cd /tmp

wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py

cd $APP_DIR
pip3 install -r requirements.txt -r prod_requirements.txt


# setup systemd services
cp packer/files/*.service /lib/systemd/system/
