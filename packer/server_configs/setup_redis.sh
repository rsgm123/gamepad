#!/usr/bin/env bash


TMP_DIR=/tmp/gamepad
APP_DIR=/home/django/gamepad
SSH_DIR=/home/django/.ssh

cd $APP_DIR

sudo apt-get update
sudo apt-get install -y redis-server

# setup redis config
sudo cp packer/files/redis.conf /etc/redis/redis.conf
sudo chown redis:redis /etc/redis/redis.conf
sudo chmod 640 /etc/redis/redis.conf
