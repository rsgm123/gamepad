"""gamepad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.views.generic.base import TemplateView
from rest_framework import routers

from gamepad import consumers
from gamepad import views

router = routers.SimpleRouter()

urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^api/groups/$', views.GroupViewSet.as_view()),

    url(r'^api/', include(router.urls)),
    url(r'^.*', TemplateView.as_view(template_name='index.html', extra_context={'debug': settings.DEBUG})),
]

websocket_urlpatterns = [
    url(r'^ws/group/(?P<group_id>\w+)/$', consumers.GroupConsumer),
    url(r'^ws/group/(?P<group_id>\w+)/host/$', consumers.HostConsumer),
    url(r'^ws/healthcheck/$', consumers.HealthCheckConsumer),
]

application = ProtocolTypeRouter({
    # (http->django views is added by default)

    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(websocket_urlpatterns)
        )
    ),
})
