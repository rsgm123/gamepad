import random
import string

from rest_framework import viewsets, serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from gamepad.models import Group


# class GameSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Game
#         fields = ()
#         read_only_fields = ('__all__',)
#         depth = 0
#

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)
        read_only_fields = ('__all__',)
        depth = 0


# class GameViewSet(viewsets.ReadOnlyModelViewSet):
#     serializer_class = GameSerializer
#     queryset = Game.objects.all()


class GroupViewSet(APIView):
    def post(self, request, **kwargs):
        unique_id = str(random.randint(1000, 9999))

        group = Group(name=unique_id)

        serializer = GroupSerializer(group)
        return Response(serializer.data)
