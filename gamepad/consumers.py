import logging
from json import dumps

from channels.generic.websocket import AsyncJsonWebsocketConsumer, AsyncWebsocketConsumer

logger = logging.getLogger(__name__)


def player_group(group_id):
    return 'group-{}'.format(group_id)


def host_group(group_id):
    return 'group-{}-host'.format(group_id)


class GroupConsumer(AsyncJsonWebsocketConsumer):
    group_id = None

    async def connect(self):
        self.group_id = self.scope['url_route']['kwargs']['group_id']

        # rate limit
        await self.accept()
        group = player_group(self.group_id)
        await self.channel_layer.group_add(group, self.channel_name)

        await self.channel_layer.group_send(host_group(self.group_id), {
            'type': 'player_message',
            'text': dumps({
                'event': 'playerJoined',
                'channelName': self.channel_name,
                'data': {},
            }),
        })

        await self.send_json({'event': 'joinedGroup', 'data': {}, 'channelName': self.channel_name})

        print('player accepted')

    async def host_message(self, event):
        await self.send(text_data=event['text'])

    async def kick_player(self, event):
        await self.close()

    # we don't really need json decodes here
    async def receive(self, text_data=None, bytes_data=None, **kwargs):
        if text_data:
            print(text_data)
            await self.channel_layer.group_send(host_group(self.group_id), {
                'type': 'player_message',
                'text': text_data,
            })

    async def disconnect(self, close_code):
        # kick player from hosts

        await self.channel_layer.group_send(host_group(self.group_id), {
            'type': 'player_message',
            'text': dumps({
                'event': 'playerDisconnected',
                'channelName': self.channel_name,
                'data': {},
            }),
        })

        await self.channel_layer.group_discard(player_group(self.group_id), self.channel_name)


class HostConsumer(AsyncJsonWebsocketConsumer):
    group_id = None

    async def connect(self):
        self.group_id = self.scope['url_route']['kwargs']['group_id']

        # rate limit
        await self.accept()
        await self.channel_layer.group_add(host_group(self.group_id), self.channel_name)
        await self.send_json({'event': 'wsReady', 'data': {}})
        print('host accepted')

    async def player_message(self, event):
        await self.send(text_data=event['text'])

    async def receive_json(self, content, **kwargs):
        print(content)
        channel = None

        if 'channelName' in content and content['channelName']:
            channel = content['channelName']

        if content['event'] == 'kickPlayer':
            await self.channel_layer.send(channel, {
                'type': 'kick_player',
                'text': dumps(content),
            })

        elif channel:
            await self.channel_layer.send(channel, {
                'type': 'host_message',
                'text': dumps(content),
            })

        else:
            await self.channel_layer.group_send(player_group(self.group_id), {
                'type': 'host_message',
                'text': dumps(content),
            })

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(host_group(self.group_id), self.channel_name)


class HealthCheckConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.accept()
        await self.send('connected')
        logger.debug('Health check consumer connected')

    async def receive(self, text_data=None, bytes_data=None):
        await self.send('pong')
        logger.debug('Health check ping')

    async def disconnect(self, close_code):
        logger.debug('Health check consumer disconnected')
