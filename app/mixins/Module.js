export default {
  props: {
    options: {type: Object, required: false},
  },

  filters: {
    upper(value) {
      return value.toUpperCase()
    },
  },

  data() {
    return {
      buttonsDown: {},
    }
  },

  methods: {
    keyDown(key) {
      if (!this.buttonsDown[key]) {
        this.buttonsDown[key] = true;
        window.navigator.vibrate(20);
        this.$player.send('controllerInput', {action: 'keyDown', key: key})
      }
    },
    keyUp(key) {
      if (this.buttonsDown[key]) {
        this.buttonsDown[key] = false;
        this.$player.send('controllerInput', {action: 'keyUp', key: key})
      }
    },
    keyPressed(key) {
      if (!this.buttonsDown[key]) {
        this.$player.send('controllerInput', {action: 'keyPressed', key: key})
      }
    },
  },
}
