import Module from "./Module";

export default {
  mixins: [Module],

  data() {
    return {
      moduleTransform: 'scale(0)',
    }
  },

  created() {
    this.$parent.$on('resize-modules', this.updateModuleTransform);
  },

  mounted() {
    this.$nextTick(this.updateModuleTransform);
  },

  destroyed() {
    this.$parent.$off('resize-modules', this.updateModuleTransform)
  },


  methods: {
    /**
     * Used to translate and scale
     *
     * @returns {string} svg transform attribute string
     */
    updateModuleTransform() {
      if (!this.$refs.svg) {
        return ''
      }

      const width = this.$refs.svg.clientWidth;
      const height = this.$refs.svg.clientHeight;

      let x = 0;
      let y = 0;
      let scale;

      if (width < height) {
        scale = width / 100;
        y = height / 2 - 50 * scale;
      } else {
        scale = height / 100;
        x = width / 2 - 50 * scale;
      }

      console.log([width, height, x, y]);

      // translate then scale, otherwise coordinates would be scaled
      // note: transform functions and function parameters are separated by spaces
      this.moduleTransform = `translate(${x} ${y}) scale(${scale})`
    },
  },
}
