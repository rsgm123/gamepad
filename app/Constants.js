import axios from 'axios'


// export const PROTOCOL = process.env.NODE_ENV !== 'production' ? 'ws' : 'wss';
export const PROTOCOL = 'ws';
export const PORT = process.env.NODE_ENV !== 'production' ? undefined : undefined; // ignores undefined

// set axios config
export const AXIOS = axios.create({
  timeout: 10000,
});

export const GAMES = [
  {
    title: 'GP Speedway',
    name: 'gp_speedway',
    description: '1-8 player racing game.',
    image: '',
  },
  {
    title: 'Moon Lander',
    name: 'moon_lander',
    description: '2-8 player spaceship physics game.',
    image: '',
  },
];
