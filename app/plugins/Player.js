import {store} from '../App'
import {PORT, PROTOCOL} from "../Constants";

// exported api for connecting, sending and registering a messageHandler
const playerRTC = {
  connect,
  disconnect,
  send,
  messageHandler: undefined,
};

let ws; // server websocket object
let channelName; // ws channelName, used as the playerId
let host; // host rtc peer connection and data channel
let game; // game rtc peer connection and data channel


function newHostRTCConnection(description, resolve, reject) {
  // create peer connection and data channel objects
  const peerConnection = new RTCPeerConnection();
  const dataChannel = peerConnection.createDataChannel('host', {negotiated: true, id: 0});
  host = {
    peerConnection,
    dataChannel,
  };

  // setup callbacks
  dataChannel.onopen = (event) => {
    console.log('host rtc connected successfully');
    resolve();
  };
  dataChannel.onmessage = rtcMessageHandler;
  dataChannel.onclose = console.error; // ?
  dataChannel.onerror = (error) => {
    console.error(error);
    reject(error);
  };

  // setup peer connection ice handler
  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      wsSend('rtcIceCandidate', event.candidate);
      console.log('host ice candidate sent');
    }
  };

  peerConnection.setRemoteDescription(description).then(() => {
    return peerConnection.createAnswer();
  }).then(answer => {
    return peerConnection.setLocalDescription(answer);
  }).then(() => {
    wsSend('rtcAnswer', peerConnection.localDescription);
    console.log('host answer sent')
  }).catch((error) => {
    console.error(error);
    reject(error);
  });
}


// similar to newHostRTCConnection, except no promise is returned and it uses different event names
function newGameRTCConnection(description) {
  // create peer connection and data channel objects
  const peerConnection = new RTCPeerConnection();
  let dataChannel = peerConnection.createDataChannel('host', {negotiated: true, id: 0});
  game = {
    peerConnection,
    dataChannel,
  };

  // setup callbacks handlers
  dataChannel.onopen = (event) => {
    console.log('game rtc connected successfully');
    send('playerJoinedGame')
  };
  dataChannel.onmessage = rtcMessageHandler;
  dataChannel.onclose = () => game = undefined;
  dataChannel.onerror = console.error;

  // setup peer connection ice handler
  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      wsSend('gameRTCIceCandidate', event.candidate);
      console.log('game ice candidate sent');
    }
  };

  peerConnection.setRemoteDescription(description).then(() => {
    return peerConnection.createAnswer();
  }).then(answer => {
    return peerConnection.setLocalDescription(answer);
  }).then(() => {
    wsSend('gameRtcAnswer', peerConnection.localDescription);
    console.log('game answer sent')
  }).catch(console.error);
}


/**
 * Connects to the group a websocket, then establishes an RTC peer connection with the host.
 * Returns a promise which resolves only if the RTC connection is successful.
 *
 * @param path
 */
function connect(path) {
  return new Promise((resolve, reject) => {
    const url = new URL('/ws' + path, location);
    url.protocol = PROTOCOL;
    if (PORT) {
      url.port = PORT;
    }

    ws = new WebSocket(url.href);
    console.log('connecting to group ws: ' + url.href);

    ws.onopen = () => console.log('ws connected, waiting for host rtc offer');
    ws.onmessage = wsMessageHandler(resolve, reject);
    ws.onclose = (error) => console.error('socket closed');
    ws.onerror = (error) => {
      console.error(error);
      reject(error);
    }
  })
}


/**
 * Disconnects from server websocket and the host RTC peer connection.
 */
function disconnect() {
  if (ws) {
    ws.close();
    ws = undefined;
  }
  if (host) {
    host.peerConnection.close();
    host.dataChannel.close();
    host = undefined;
  }
  if (game) {
    game.peerConnection.close();
    game.dataChannel.close();
    game = undefined;
  }
  channelName = undefined;
}


/**
 * Sends a message to a websocket.
 * This takes an object and serializes it as json text.
 *
 * @param event message event
 * @param data message data
 */
function wsSend(event, data) {
  ws.send(JSON.stringify({event, data, channelName}))
}

/**
 * Sends a message to the host via dataChannel.
 * This takes an object and serializes it as json text.
 *
 * @param event message event
 * @param data message data
 */
function send(event, data) {
  const message = JSON.stringify({event, data});

  if (game) {
    game.dataChannel.send(message);
  } else {
    host.dataChannel.send(message);
  }
}


/**
 * This sets up the websocket messageHandler.
 * The purpose of this handler is to react to game invites and setup RTC connections.
 * This function is nested and calling the outer one saves the resolve/reject function references for after the RTC connection is set up.
 *
 * @param resolve promise resolve
 * @param reject promise reject
 * @return {Function} message handler function
 */
function wsMessageHandler(resolve, reject) {
  return (event) => {
    const message = JSON.parse(event.data);
    console.log(message);

    if (message.event === 'gameInvite' && !game) { //
      wsSend('playerReady', store.state.player)

    } else if (message.event === 'rtcOffer') {
      channelName = message.channelName; // do this for rtc offers so we don't have to clear it when we leave a group
      newHostRTCConnection(message.data, resolve, reject);

    } else if (message.event === 'rtcIceCandidate') { // accept RTC ICE Candidates
      host.peerConnection.addIceCandidate(message.data);
      console.log('host ice candidate added')

    } else if (message.event === 'gameHostRtcOffer') {
      channelName = message.channelName;
      newGameRTCConnection(message.data);

    } else if (message.event === 'gameHostRtcIceCandidate') { // accept RTC ICE Candidates
      game.peerConnection.addIceCandidate(message.data);
      console.log('game ice candidate added')
    }
  }
}


/**
 * RTC message handler used as the callback for both the the host and game data channels.
 * This first reacts to some predefined events, otherwise this will call the playerRTC.messageHandler, if set.
 *
 * @param event rtc message event
 */
function rtcMessageHandler(event) {
  const message = JSON.parse(event.data);
  console.log(message);

  if (message.event === 'setController') {
    store.commit('setController', message.data)

  } else if (message.event === 'gameClosed') {
    if (game) {
      game.peerConnection.close();
      game.dataChannel.close();
      game = undefined;
      console.log('game closed')
    }

  } else if (playerRTC.messageHandler) { // other message handlers
    playerRTC.messageHandler(message)
  }
}


export default {
  install: function (Vue, options) {
    Vue.player = playerRTC;
    Vue.prototype.$player = playerRTC;
  }
}
