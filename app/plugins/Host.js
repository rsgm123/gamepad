import {store, router} from '../App'
import {PORT, PROTOCOL} from "../Constants";

const hostRTC = {
  connect,
  disconnect,
  send,
  messageHandler: undefined,
};

let ws;
let players = {};


function newRTCConnection(channelName) {
  // create peer connection and data channel objects
  const peerConnection = new RTCPeerConnection();
  const dataChannel = peerConnection.createDataChannel('group', {negotiated: true, id: 0});
  players[channelName] = {
    peerConnection,
    dataChannel,
  };

  // setup message handlers
  dataChannel.onopen = () => console.log('player rtc connected successfully');
  dataChannel.onmessage = rtcMessageHandler(channelName);
  dataChannel.onerror = console.error;

  // setup peer connection ice handler
  peerConnection.onicecandidate = event => {
    if (event.candidate) {
      wsSend('rtcIceCandidate', event.candidate, channelName);
      console.log('ice candidate sent');
    }
  };

  // start the RTCPeerConnection negotiation flow
  peerConnection.createOffer().then(offer => {
    return peerConnection.setLocalDescription(offer)
  }).then(() => {
    wsSend('rtcOffer', peerConnection.localDescription, channelName);
    console.log('offer sent')
  }).catch((error) => console.error);
}


function connect(path) {
  return new Promise((resolve, reject) => {
    const url = new URL('/ws' + path, location);
    url.protocol = PROTOCOL;
    if (PORT) {
      url.port = PORT;
    }

    ws = new WebSocket(url.href);
    console.log('connecting to group ws: ' + url.href);

    ws.onopen = resolve;
    ws.onmessage = wsMessageHandler;
    ws.onclose = (error) => console.error('socket closed');
    ws.onerror = (error) => {
      console.error(error);
      reject();
    };
  });
}


/**
 * Disconnects from server websocket and all player RTC peer connections.
 */
function disconnect() {
  if (ws) {
    ws.close();
    ws = undefined
  }

  for (player of Object.values(players)) {
    player.peerConnection.close();
    player.dataChannel.close();
  }
  players = {};
}


/**
 * Sends a message to a websocket.
 * This takes an object and serializes it as json text.
 *
 * @param event message event
 * @param data message data
 * @param channelName which player to sent to, optional
 */
function wsSend(event, data, channelName) {
  ws.send(JSON.stringify({event, data, channelName: channelName}))
}

/**
 * Sends a message to a player dataChannel.
 * This takes an object and serializes it as json text.
 *
 * @param event message event
 * @param data message data
 * @param channelName which player to sent to, optional
 */
function send(event, data, channelName) {
  const message = {event, data};

  if (channelName) {
    players[channelName].dataChannel.send(JSON.stringify(message))
  } else {
    Object.values(players).forEach(player => player.dataChannel.send(JSON.stringify(message)))
  }
}


/**
 * Websocket messageHandler callback.
 * The purpose of this handler is to react to joining players and setup RTC connections.
 */
function wsMessageHandler(event) {
  const message = JSON.parse(event.data);
  const channelName = message.channelName;
  console.log(message);

  if (message.event === 'playerJoined') {
    // if (router.currentRoute.name === 'play') {
    //   wsSend('kickPlayer', {}, message.channelName);
    // }
    newRTCConnection(channelName);

  } else if (message.event === 'rtcAnswer') { // respond to RTC Answers
    const player = players[channelName];
    player.peerConnection.setRemoteDescription(message.data);
    console.log('answer received')

  } else if (message.event === 'rtcIceCandidate') { // accept RTC ICE Candidates
    const player = players[channelName];
    player.peerConnection.addIceCandidate(message.data);
    console.log('player ice candidate added')

  } else if (message.event === 'playerDisconnected') {
    console.error('player disconnected: ' + message.data.channelName);
    store.commit('removePlayer', message.channelName);
  }
}


/**
 * RTC message handler used as the callback for both the the player data channels.
 * This first reacts to some predefined events, otherwise this will call the playerRTC.messageHandler, if set.
 * This is a nested function so that we can know which player channel name is receiving the message
 *
 * @param channelName the player data channel to which this messageHandler belongs
 * @return {Function} message handler function
 */
function rtcMessageHandler(channelName) {
  return (event) => {
    const message = JSON.parse(event.data);
    console.log(message);

    if (message.event === 'playerReady') {
      store.commit('addPlayer', {
        name: message.data.name,
        color: message.data.color,
        channelName: channelName,
      });

    } else if (hostRTC.messageHandler) { // other message handlers
      hostRTC.messageHandler(channelName, message)
    }
  }
}


export default {
  install: function (Vue, options) {
    Vue.host = hostRTC;
    Vue.prototype.$host = hostRTC
  }
}
