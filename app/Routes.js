import Dashboard from './views/Dashboard.vue'

import GroupStart from './views/GroupStart.vue'
import GroupJoin from './views/GroupJoin.vue'

import Host from './views/Host.vue'
import Controller from './views/Controller.vue'
import Play from './views/Play.vue'
import GameSelect from './views/GameSelect.vue';


export default [
  { // just teases the games
    path: '/',
    name: 'dashboard',
    component: Dashboard,
  },

  { // create or join a group
    path: '/start',
    name: 'groupStart',
    component: GroupStart,
  },
  { // join a group
    path: '/join/:group',
    name: 'groupJoin',
    component: GroupJoin,
  },

  { // player controller page
    path: '/controller',
    name: 'controller',
    component: Controller,
  },

  { // host screen
    path: '/group/:group',
    name: 'host',
    component: Host,
    children: [
      {
        path: '/games',
        name: 'groupGameSelect',
        component: GameSelect,
      },
      {
        path: '/play/:game',
        name: 'play',
        component: Play,
      },
    ],
  },
]
