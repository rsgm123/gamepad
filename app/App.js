import 'vuetify/dist/vuetify.min.css'

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import Host from "./plugins/Host";
import Player from "./plugins/Player";

import routes from './Routes.js'
import vuexStore from './Store.js'

import Navbar from './components/Navbar.vue'
import Snackbar from './components/Snackbar.vue'

import colors from 'vuetify/es5/util/colors'



const observer = new PerformanceObserver((entryList) => console.debug(entryList.getEntries()));
observer.observe({entryTypes: ["measure", 'mark', 'resource', 'paint']});

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(Host);
Vue.use(Player);
// Vue.use(Websockets);
// Vue.use(RTC);

Vue.use(Vuetify, {
  theme: {
    primary: colors.blue.darken1,
    secondary: colors.green.accent3,
    accent: colors.orange.accent3,
    error: colors.red.accent3
  }
});


export const store = new Vuex.Store(
    vuexStore
);


export const router = new VueRouter({
  mode: 'history',
  routes
});


const app = new Vue({
  router,
  store,

  components: {
    Navbar,
    Snackbar,
  },
}).$mount('#app');
