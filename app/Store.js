import Vue from 'vue'

export default {
  strict: true,

  state: {
    group: {
      id: undefined,
      isHost: undefined,
    },

    player: {
      name: undefined,
      color: undefined,
    },

    host: {
      players: {},
    },

    controller: {
      'vertical': false,
      'modules': [
        {'module': 'DPadOne'},
      ]
    },
  },

  mutations: {
    setGroup(state, {id, isHost}) {
      state.group.id = id;
      state.group.isHost = isHost;
    },

    setPlayer(state, {name, color}) {
      state.player.name = name;
      state.player.color = color;
    },

    addPlayer(state, {name, color, channelName}) {
      Vue.set(state.host.players, channelName, {name, color, channelName})
    },

    removePlayer(state, channelName) {
      Vue.delete(state.host.players, channelName)
    },

    setController(state, controller) {
      state.controller = controller // may need to use $set
    },
  },

  actions: {
    joinGroup({dispatch, state, commit}, {groupId, name, color}) {
      return Vue.player.connect(`/group/${groupId}/`).then(() => {
        console.log(['connected', groupId]);
        commit('setGroup', {id: groupId, isHost: false});
        commit('setPlayer', {name, color});
        return state.player
      })
    },

    hostGroup({dispatch, commit}, groupId) {
      return Vue.host.connect(`/group/${groupId}/host/`).then(() => {
        console.log(['connected', groupId]);
        commit('setGroup', {id: groupId, isHost: true});
      })
    }
  },

  getters: {
    joinUrl: (state, getters) => new URL(`/join/${state.group.id}`, location).href,
    players: (state, getters) => Object.values(state.host.players),
  },
}
