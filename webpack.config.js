const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = function (env) {
  return {
    entry: {
      build: path.resolve(__dirname, 'app/App.js'),
    },

    output: {
      path: path.resolve(__dirname, 'assets'),
      filename: '[name].js'
    },

    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          exclude: /node_modules|venv|server/,
        },

        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /venv|server/,
        },

        {
          test: /\.(css)$/,
          loaders: ["style-loader", "css-loader?sourceMap"],
          exclude: /venv|server/, // don't exclude node modules
        },

        {
          test: /\.(styl)$/,
          loader: 'css-loader!stylus-loader',
          exclude: /venv|server/, // don't exclude node modules
        },

        {
          test: /\.(png|jpg|gif|svg)$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]?[hash]'
          }
        },
      ]
    },

    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      }
    },

    devServer: {
      index: path.resolve(__dirname, 'templates/index.html'),
      publicPath: '/static/', // where build.js is served from
      host: '0.0.0.0',
      proxy: [{
        context: ['**', '!/static/build.js', '!/sockjs-node/**'],
        target: 'http://localhost:5000/',
        changeOrigin: true,
        ws: true,
      }],
      historyApiFallback: true,
      noInfo: true
    },

    performance: {
      hints: false
    },

    mode: env && env.prod ? 'production' : 'development',
    devtool: "source-map", // uses a lot of memory, enable if needed

    plugins: plugins(env),
  };
};


function plugins(env) {
  let plugins = [
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new VueLoaderPlugin()
  ];

  // if (env && env.prod) {
  //   // const MinifyPlugin = require("babel-minify-webpack-plugin");
  //
  //   plugins = (plugins || []).concat([
  //     new webpack.DefinePlugin({
  //       'process.env': {
  //         NODE_ENV: '"production"'
  //       }
  //     }),
  //
  //     new MinifyPlugin({}, {sourceMap: false}),
  //
  //     new webpack.LoaderOptionsPlugin({
  //       minimize: true
  //     })
  //   ])
  // }

  return plugins
}
