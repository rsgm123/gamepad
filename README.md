GamePad
=======

Gamepad is a party game platform.
Gamepad lets you play full multiplayer games through the browser using mobile phones as controllers.
The idea is that you put game on a TV and everyone connects and plays through their phones.

Players connect to a group via websockets, then establish a webRTC connection to the host.
This allows GamePad to work with very little traffic and very low latency.

The platform is still in development, however the necessary components to setup games and connect players are all working.
The platform itself is mainly a Vue.js app with a small django/DRF backend.
The backend is not much more than a websocket relay, however it uses DRF because I some plans to expand it in the future.

The games that I have written are made using Godot, so I have included scripts that may be dropped into Godot games that will automatically setup the networking to the players with very little configuration.
Games written with other engines/languages are fine, but you will have to implement networking yourself.

To run this in debug, simply run:
```
./manage.py runserver 0.0.0.0:5000
npm run dev
```

Otherwise, there is a server running at `http://51.15.227.55.xip.io/` until I get a good domain name.
