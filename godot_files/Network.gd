# GamePad Networking script
# updated 2019-10-26
#
# purpose: creates a connection to the server, then facilitates setup of webRTC peer connections to player controllers
#
# This emits signals on any peer message and input
# usage: autoload as 'Network'
extends Node

signal input
signal message
signal ws_ready

const PLAYER = preload('res://Player.gd')

var ws = null
var server = null
const packets = []


const players = {}


func _ready():
    _connect_ws()


func _connect_ws():
    """
    Establishes a new WS connection.
    """
    server = null
    packets.clear()

    for player in players.values():
        player.queue_free()  # this may or may not close the connection
    players.clear()

    ws = WebSocketClient.new()
    ws.connect('connection_established', self, '_connected')
    ws.connect('data_received', self, '_data_recieved')
    ws.connect('connection_error', self, '_error')
    ws.connect('server_close_request', self, '_closed_request')
    ws.connect('connection_closed', self, '_closed')

#    var regex = RegEx.new()
#    regex.compile("^((http[s]?):\\/)?\\/?([^:\\/\\s]+)((\\/\\w+)*\\/)([\\w\\-\\.]+[^#?\\s]+)(.*)?(#[\\w\\-]+)?$")
#    var result = regex.search(JavaScript.eval('location.href'))
    var host = JavaScript.eval('location.host')
    var search = JavaScript.eval('location.search')

#    if not result:
#        printerr('malformed url')

    print('host: %s, search: %s' % [host, search])
    var group_id = search.split('=')[1]

    # wss when we turn on tls/https
    var url = 'ws://%s/ws/group/%s/host/' % [host, group_id]

    print('Connecting to ' + url)
    ws.connect_to_url(url)


func _server_ready():
    """
    Called once the WS connection is ready to send data.
    This marks the connection as ready by setting the 'server' variable.
    This then sends any data waiting to be sent and sends out game invites.
    """
    print('server_ready')

    server = ws.get_peer(1)
    server.set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)

    # send packets waiting for connection
    while len(packets) > 0:
        server.put_packet(packets.pop_front())

    emit_signal('ws_ready')


func _connected(protocol):
    """
    Called when the WS connection is opened.
    However, the connection is not ready for sending data until we recieve data from the server.
    """
    print('connected')


func _data_recieved():
    """
    Called when new data is sent from a player.
    This will get and deserialize new data from of the server object.
    This is also used to mark the WS connection as ready to send data.
    """
    print('data recieved')
    if not server:
        _server_ready()

    while server.get_available_packet_count() > 0:
        var packet = server.get_packet()
        var json = packet.get_string_from_utf8()
        print('recieve %s' % json)
        _handle_message(JSON.parse(json).result)


func _error():
    printerr('connection error')


func _closed_request(code, reason):
    print('connection close request')


func _closed(was_clean_close):
    """
    Called when the connection to the server is closed.
    This will attempt to reconnect to the server in 5 seconds.
    This will also be called each time a new connection fails.
    """
    print('connection closed, attempting to reconnect in 5 seconds')

    yield(get_tree().create_timer(5), 'timeout')
    _connect_ws() # todo: what happens if we are in a game?


func _handle_message(content):
    """
    Handles player messages as events, and emits signals.
    This responds to a few specific signals to setup the networking.
    """
    var player_id = content.get('channelName', '')
    var event = content['event']
    var data = content.get('data', null)

    if event == 'wsReady':
        pass  # nothing to do

    if event == 'gameRtcAnswer':
        players[player_id].answer(data)

    if event == 'gameRTCIceCandidate':
        players[player_id].ice_candidate(data)

    if event == 'playerReady':
        print('player %s joined' % player_id)

        var player = PLAYER.new()
        add_child(player)  # needs to be added to the scene tree

        players[player_id] = player  # store player
        player.data.id = player_id
        player.data.name = data['name']
        player.data.color = data['color']
        player.connect_rtc()


func wsSend(event, data, player_id=null):
    """
    Sends data via websockets to a single player or all players.
    This should only be used during RTC setup
    Data must be a dict.
    """
    var packet = JSON.print({'event': event, 'channelName': player_id, 'data': data}).to_utf8()

    if server:
        server.put_packet(packet)

    else:
        packets.append(packet)


func send(event, data, player_id=null):
    """
    Sends data via RTC to a single player or all players.
    Player.send() may be used in place of this
    Data must be a dict.
    """
    if player_id:
        players[player_id].send(event, data)
    else:
        for player in players.values():
            player.send(event, data)
