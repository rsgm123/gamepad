# GamePad utilities script
# updated 2019-09-28
#
# purpose: holds constants and functions shared across all games
# usage: autoload as 'Utils'
extends Node

# Player colors map from names to rgb values
# player colors are sent as names
const PLAYER_COLORS = {
    'red': 'F44336',
    'purple': '9C27B0',
    'indigo': '3F51B5',
    'light-blue': '03A9F4',
    'teal': '009688',
    'light-green': '8BC34A',
    'yellow': 'FFEB3B',
    'orange': 'FF9800',
}


# used in the enumerate function
class Enumerate:
    var index
    var object

# similar to python's built-in enumerate function
func enumerate(array):
    var new_array = []
    for i in range(len(array)):
        var enumerate = Enumerate.new()
        enumerate.index = i
        enumerate.object = array[i]
        new_array.append(enumerate)
    return new_array
