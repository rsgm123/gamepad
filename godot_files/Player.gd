# GamePad Player script
# updated 2019-10-18
#
# purpose: creates and handles an RTC connection to an individual player, also holds any player specific data needed
#
# This emits signals on peer message and input from this player
# usage: no need to autoload, but keep this at 'res://Player.gd'
extends Node

signal input
signal message

var peer_connection = null
var data_channel = null
var is_ready = false
const packets = []  # packets waiting for the connection to be ready

var data # holds PlayerData, defined below


class PlayerData extends Object:
    var id
    var name
    var color

    # add custom data here
    var ready = false


func _ready():
    data = PlayerData.new()


func connect_rtc():
    data_channel = null
    is_ready = false
    packets.clear()

    print('Connecting to peer' + data.id)
    peer_connection = WebRTCPeerConnection.new()
    data_channel = peer_connection.create_data_channel('game', {'negotiated': true, 'id': 0})  # todo: try using an unreliable channel

    peer_connection.connect('session_description_created', self, '_session_description_created')
    peer_connection.connect('ice_candidate_created', self, '_ice_candidate_created')
    peer_connection.create_offer()


func _reconnect():
    print('connection closed, attempting to reconnect peer in 5 seconds')
    yield(get_tree().create_timer(5), 'timeout')
    connect_rtc()


func answer(data):
    print('Got answer from offer')
    peer_connection.set_remote_description('answer', data['sdp'])

func ice_candidate(data):
    print('Got answer from offer')
    peer_connection.add_ice_candidate(data['sdpMid'], data['sdpMLineIndex'], data['candidate'])


func _session_description_created(type, sdp):
    print('session_description_created')
    peer_connection.set_local_description(type, sdp)
    Network.wsSend('gameHostRtcOffer', {'type': type, 'sdp': sdp}, data.id)


func _ice_candidate_created(media, index, name_):
    print(['ice_candidate_created', media, index, name_])
    Network.wsSend('gameHostRtcIceCandidate', {'sdpMid': media, 'sdpMLineIndex': index, 'candidate': name_}, data.id)


func _process(delta):
    if peer_connection:
        peer_connection.poll()
        match peer_connection.get_connection_state():
            WebRTCPeerConnection.STATE_NEW:
                print('STATE_NEW')
            WebRTCPeerConnection.STATE_CONNECTING:
                print('STATE_CONNECTING')
#            WebRTCPeerConnection.STATE_CONNECTED:
#                print('STATE_CONNECTED')
            WebRTCPeerConnection.STATE_DISCONNECTED:
                print('STATE_DISCONNECTED')
            WebRTCPeerConnection.STATE_FAILED:
                print('STATE_FAILED')
            WebRTCPeerConnection.STATE_CLOSED:
                print('STATE_CLOSED')
                _reconnect()

    if data_channel:
        match data_channel.get_ready_state():
            WebRTCDataChannel.STATE_CONNECTING:
                print('STATE_CONNECTING')
            WebRTCDataChannel.STATE_OPEN:
#                print('STATE_OPEN')
                if not is_ready:
                    _connection_ready()
            WebRTCDataChannel.STATE_CLOSING:
                print('STATE_CLOSING')
            WebRTCDataChannel.STATE_CLOSED:
                print('STATE_CLOSED')

        while data_channel.get_available_packet_count() > 0:
            var json = data_channel.get_packet().get_string_from_utf8()
            print('recieve %s' % json)
            _handle_message(JSON.parse(json).result)


func _connection_ready():
    print('server_ready')
    is_ready = true
    data_channel.set_write_mode(WebRTCDataChannel.WRITE_MODE_TEXT)

    # send packets waiting for connection
    while len(packets) > 0:
        data_channel.put_packet(packets.pop_front())


func _handle_message(content):
    """
    Handles player messages as events, and emits signals.
    """
    var event = content['event']

    if event == 'controllerInput':
        self.emit_signal('input', self, content)
        Network.emit_signal('input', self, content)
    else:
        self.emit_signal('message', self, content)
        Network.emit_signal('message', self, content)


func send(event, data):
    """
    Sends data to a single player or all players.
    Data must be a dict.
    """
    var packet = JSON.print({'event': event, 'data':data}).to_utf8()
    if is_ready:
        data_channel.put_packet(packet)
    else:
        packets.append(packet)
